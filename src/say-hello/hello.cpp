#include <config.h>
#include <iostream>
using namespace std;

int main() {
  cout << "Hello There" << endl;

#ifdef function
  cout << "Config.h file has been included to the main code";
#endif

  return 0;
}